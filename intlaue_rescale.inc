C##INCLUDE## DIMGNS 
        DIMENSION IREC(100000),IX(100000),IY(100000),IRXY(3)
        PARAMETER (MAXRFL=100000)
C####END####
C##INCLUDE## COMBCK 
C
C
C             COMMON FOR ROSSMANN BACKGROUND PLANES
C                   AND ROSSMAN PROFILES.
C
C
        COMMON /BGBIN/ BN, BP, BQ, PS, QS, MS,
	1           REJ, SIGMA,BO,PODSUM,QODSUM,PN,PPSUM, 
	2           BPQ, BPP, BQQ, BOP, BOQ, A, BZ, C,QQSUM,
	3           AV(3,2), NSAT,NNEG,NOK(2),TOTAL(2),
	4           CUTU,CUTL,IMF,ICLIP,IMS,DSX,DSY,RMAX,
	5           SS,SMS,AMPRO,AMST,CHANGE,FCUT,NMBIN
C
C
	INTEGER X(18), Y(18), BN(18), BP(18), BQ(18),
	1       PS(18), QS(18), MS(18),
	2       REJ(18),SIGMA(18),PN
C
	REAL    BPQ(18), BPP(18), BQQ(18), BOP(18),
	1       BOQ(18), A(18), BZ(18), C(18),
	2       SS(18), SMS(18),BO(18),
	3       AMPRO(3,3),AMST(3,3),CUTU,CUTL,
	4	PODSUM,QODSUM,PPSUM,QQSUM
C
	LOGICAL CHANGE(18)
C
C
C####END####
C##INCLUDE## COMFID 
        COMMON /FID/ XCENF,YCENF,OMEGAF,MM,FSPOS(4,2),
     1  NFID,CCX,CCY,CCOM,DTOFD,WFILM,IYOFF,IYLEN,
     1	X_CEN_FS(6), Y_CEN_FS(6), W_FS(6), YSCGEN, CENT_IN
        INTEGER XCENF,YCENF,MM,FSPOS,CCX,CCY,NFID,WFILM,IYOFF,IYLEN
        REAL OMEGAF,CCOM,DTOFD,X_CEN_FS,Y_CEN_FS,W_FS,YSCGEN
	LOGICAL CENT_IN
C####END####
C##INCLUDE## COMHAT 
C             COMMON  HAT-MATRIX AND REJECTION OF POINTS
C               IN ROSSMANN BACKGROUND PLANES AND OVERLAPS.
C
C
      COMMON /HBCKGD/HATB(5000,3),HREJ(5000),HCUT,HNOBS
      COMMON /HOVLP/HATP(5000,5,17),HOVLAP,HPOBS(17),OREJ
      COMMON /BOTH/HAT(5000,3),H(5000),PORB
      COMMON /HLOG/OVLAP,CORRECT,ENDBIN,HATBG
	common/newh/ hsp(5000,17),prorej,nopuse,ratrej,
     *               ogdrej,rgdrej
C
C
      INTEGER HREJ,HNOBS,HN,HPOBS,PORB,OREJ,
     * prorej,nopuse,ratrej,ogdrej,rgdrej
      REAL HCUT,HOVLAP
      BYTE IDEC
      common/ovflag/idec(100000)
C
      LOGICAL OVLAP,CORRECT,ENDBIN,HATBG
C####END####
C##INCLUDE## COMLAUE 
c     laue common for storing complete list of x,y
c
c     nodal stores the packed values: nodal value, imult, imeas, iov
c     where nodal value is 0 to 12 (in top 5 bits)
c           imult is 0/1 for singlet/multiplet
c           imeas is 0/1 for measurable/unmeasurable
c           iov is   0/1 for non spatial/ spatial
c
      byte nodal
      common/laue/numnod,nodlim1,nodlim2, nodal(100000),
     1 xyge(2,100000),nodpt(2000)
c NEW COMMON TO HANG ON TO RECORD NUMBER > 32767
c*****************************************
c      ADDED TO COMLAUE
	common/lauebb/NBBREC(100000)
C####END####
C##INCLUDE## COMMASK 
	common/ramask/irasmask,cmask,kmask,dmask,amajor,bminor,border,
     1               rmask(5000),rmaskb(18,5000),rsums(6),
     1		     rsumsb(18,6),ammax,ammin,themax, maskfile
	real kmask,rsums,rsumsb,border
	integer rmask,rmaskb
        character*32 maskfile
C####END####
C##INCLUDE## COMMASKL 
	common/maskl/streak,binvar
	logical streak,binvar
C####END####
C##INCLUDE## COMMCS 
        COMMON /MCS/ FILM,FILMS,CTOFD,PCKIDX,
     1  TOSPT,VEE,N1OD,BASEOD,G1OD,CURV,XMIN,XMAX,
     2  ymin,ymax
        INTEGER FILM,FILMS,CTOFD,PCKIDX
        INTEGER TOSPT,N1OD,XMIN,XMAX,ymin,ymax
        REAL BASEOD,G1OD,CURV
        LOGICAL VEE
C####END####
C##INCLUDE## COMORI 
        COMMON /ORI/ COSOM0,SINOM0,XTOFRA,YSCAL,
     1  XCEN,YCEN,CBAR,TILT,TWIST,BULGE,VTILT,VVERT,roff,toff,
     1  spdx_min, spdx_max, spdy_min, spdy_max, 
     1  spdx(20),spdy(20),nspdx,nspdy,dstor,spdxy
        REAL COSOM0,SINOM0,XTOFRA,YSCAL,TILT,TWIST,BULGE,VTILT,VVERT
	REAL ROFF,TOFF
        integer nspdx,nspdy,dstor
        real spdx_min,spdx_max,spdy_min,spdy_max, spdx,spdy,spdxy
        INTEGER XCEN,YCEN,CBAR
        EQUIVALENCE (TILT,VBNEG),(TWIST,VBPOS),(BULGE,VTWIST)
C####END####
C##INCLUDE## COMPRF 
	COMMON /PRF/ IP(5000,18),IRAD,NOB(18),IXBIN(18),IYBIN(18),
     1               IPJ(18),XIP(5000,18),IPSC(18),INTP,INTPRO,
     2               IPW(5000),ACONS,NPX,NPY,NPXY,SIGPRO,ISAN,
     3               NPWK,NUMBIN,IRAD2
	REAL IPSC,ACONS
	INTEGER SIGPRO
C####END####
C##INCLUDE## COMPRO 
      COMMON/PRO/IFILL,DUMM(155000)
       INTEGER DUMM
       INTEGER IFILL
       INTEGER IREC(1000),INTEN(1000),ISD(1000),MASK(5000,-15:15)
	integer intenb(1000),isdb(1000)
       EQUIVALENCE (MASK(1,-15),DUMM(1))
       EQUIVALENCE (IREC(1),DUMM(1)),(INTEN(1),DUMM(1001))
       EQUIVALENCE (ISD(1),DUMM(2001))
	equivalence (INTENb(1),DUMM(3001)),(ISDb(1),DUMM(4001))
C####END####
C##INCLUDE## COMRAS 
        COMMON /RAS/ IRAS(5),MINT,VARAS(3)
        INTEGER IRAS,MINT
        REAL VARAS
        EQUIVALENCE (IRAS(1),NXS),(IRAS(2),NYS),(IRAS(3),NC)
        EQUIVALENCE (IRAS(4),NRX),(IRAS(5),NRY)
C####END####
C##INCLUDE## COMREP  
        COMMON /REPRT/ NREF,NOFR,NOLO,MAXBSI,MINBSI,
     1  IRANGE,IANAL,RATIO,AVSD,NEDGE,NBOX,NBZERO,
     1  IBOXS,ITOTS,IPROS,NBAD,iprange
        INTEGER NREF,NOFR,NOLO,MAXBSI,MINBSI
        INTEGER IRANGE(9),IANAL(10),iprange(9) 
        REAL RATIO(10),AVSD(10)
C####END####
C##INCLUDE## COMRFS 
        COMMON /RFS/ MAXREF,MAXX,MAXY,NRS,RMSRES,XRS(512),YRS(512),
     1  RRS(512),MAXR,FDIST,ESTART
        INTEGER MAXREF,MAXX,MAXY,NRS,XRS,YRS,RRS,MAXR
        REAL RMSRES,FDIST,ESTART
C####END####
C##INCLUDE## COMSCN 
        COMMON /SCN/ SCNSZ,ACTX,FACT,NWORD
        INTEGER ACTX,NWORD
        REAL FACT,SCNSZ
C####END####
C##INCLUDE## COMSND 
        COMMON /WRSEND/ MODE, FNAME
        INTEGER MODE
	CHARACTER*20 FNAME
C####END####
C##INCLUDE## COMTEM 
       COMMON /TEMOD /IMAXOD
C####END####
C##INCLUDE## COMUAB 
	COMMON/UABCOM/LTEK,XTEK,ONLINE,PROFIL,BGPLAN,Batch,
     1         WTPRO,PROPT,I2REF,IBOTH
	LOGICAL LTEK,XTEK,ONLINE,PROFIL,BGPLAN,Batch,
     1         WTPRO,PROPT,I2REF,IBOTH
C####END####
C##INCLUDE## COMHIS 
	common/hist/histbg(12,12),histn(12,12),lastbg(12,12),
     1              histbg1(12,12),histn1(12,12),
     1              ixhbg,iyhbg,xhmin,xhmax,yhmin,yhmax
	integer histbg,histn,lastbg,histbg1,histn1
	integer xhmin,xhmax,yhmin,yhmax
	common/sfname/sfile
C####END####
**EOF**
